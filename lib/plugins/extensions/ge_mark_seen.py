# -*- coding: UTF-8 -*-

__revision__ = '$Id$'

# Copyright © 2009 Piotr Ożarowski
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published byp
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Library General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA

# You may use and distribute this software under the terms of the
# GNU General Public License, version 2 or later

import logging

from sqlalchemy.sql import update, select

from db.tables import movies as movies_table
from gutils import question
from plugins.extensions import GriffithExtensionBase as Base
from sql import update_whereclause
from quick_filter import change_filter_update_whereclause

log = logging.getLogger('Griffith')

class GriffithExtension(Base):
    name = 'Mark as seen'
    description = _('Marks all currently filtered movies as seen')
    author = 'Piotr Ożarowski'
    email = 'piotr@griffith.cc'
    version = 0.1
    api = 1
    enabled = False # disabled by default

    toolbar_icon = 'ge_mark_seen.png'

    def toolbar_icon_clicked(self, widget, movie):
        if question(_('Are you sure you want to update %d movies?') % self.app.total):
            session = self.db.Session()

            query = select([movies_table.c.movie_id])
            # add conditions from simple filter
            change_filter_update_whereclause(self.app, query)
            # add conditions from adv filter
            query = update_whereclause(query, self.app._search_conditions)
            movie_ids = []
            for movie_entry in session.execute(query):
                movie_ids.append(movie_entry.movie_id)

            update_query = update(movies_table, values={'seen': True})
            update_query = update_query.where(movies_table.c.movie_id.in_(movie_ids))
            log.debug(update_query)
            session.execute(update_query)
            session.commit()
            
            self.app.populate_treeview() # update seen widget in the list
